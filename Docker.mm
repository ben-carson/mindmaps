<map version="freeplane 1.11.1">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Docker" FOLDED="false" ID="ID_672321251" CREATED="1544718345358" MODIFIED="1588173148106" COLOR="#000000">
<font NAME="SansSerif" SIZE="20"/>
<edge WIDTH="2"/>
<hook NAME="MapStyle">
    <properties EDGECOLORCONFIGURATION="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" FIT_TO_VIEWPORT="false" SHOW_NOTE_ICONS="true" fit_to_viewport="false" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_168696578" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_168696578" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork"/>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork"/>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900"/>
<stylenode LOCALIZED_TEXT="styles.important"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="Dockerfile" POSITION="bottom_or_right" ID="ID_375841742" CREATED="1544718360627" MODIFIED="1588173162950" COLOR="#0033ff" STYLE="bubble">
<font NAME="SansSerif" SIZE="18"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<node TEXT="FROM" ID="ID_779739016" CREATED="1544719994198" MODIFIED="1588173148077" LINK="https://docs.docker.com/engine/reference/builder/#from" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
<node TEXT="RUN" ID="ID_1019302525" CREATED="1544720001365" MODIFIED="1588173148078" LINK="https://docs.docker.com/engine/reference/builder/#run" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
<node TEXT="COPY" ID="ID_781010997" CREATED="1544720011057" MODIFIED="1588173148078" LINK="https://docs.docker.com/engine/reference/builder/#copy" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
<node TEXT="CMD" ID="ID_1093798171" CREATED="1549649386312" MODIFIED="1588173148079" LINK="https://docs.docker.com/engine/reference/builder/#cmd" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
<node TEXT="EXPOSE" ID="ID_189613309" CREATED="1549649748666" MODIFIED="1588173148080" LINK="https://docs.docker.com/engine/reference/builder/#expose" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
<node TEXT="USER" ID="ID_584705776" CREATED="1549653072013" MODIFIED="1588173148081" LINK="https://docs.docker.com/engine/reference/builder/#user" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The <code class="highlighter-rouge">USER</code>&#160;instruction sets the user name (or UID) and optionally the user group (or GID) to use when running the image and for any <code class="highlighter-rouge">RUN</code>, <code class="highlighter-rouge">CMD</code>&#160; and <code class="highlighter-rouge">ENTRYPOINT</code>&#160;instructions that follow it in the <code class="highlighter-rouge">Dockerfile</code>.
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node TEXT="commands" POSITION="top_or_left" ID="ID_158629326" CREATED="1544718560082" MODIFIED="1588173148081" COLOR="#0033ff">
<font NAME="SansSerif" SIZE="18"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<node TEXT="run" FOLDED="true" ID="ID_1837448788" CREATED="1544718567985" MODIFIED="1588349378017" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl + click to open link<a href="https://docs.docker.com/engine/reference/commandline/run/">
</a>    </p>
    <p>
      <a href="https://docs.docker.com/engine/reference/commandline/run/">docker run</a>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="--rm" ID="ID_1006362855" CREATED="1544718681413" MODIFIED="1588173148082" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      remove container on exit
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--interactive, -i" ID="ID_529005182" CREATED="1544718740594" MODIFIED="1588173148083" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Keep STDIN open, even if not attached
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--tty, -t" ID="ID_1834936680" CREATED="1544718788283" MODIFIED="1588173148084" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Pseudo-TTY interface
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--name" ID="ID_411560295" CREATED="1544718817816" MODIFIED="1588173148084" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      give the container a name
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--volume, -v" ID="ID_943061967" CREATED="1544721097245" MODIFIED="1588173148085" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Bind mount a volume
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--workdir, -w" ID="ID_1741675239" CREATED="1544721249793" MODIFIED="1588173148086" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Working directory inside the container
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--detach, -d" ID="ID_1205357943" CREATED="1588349383141" MODIFIED="1588349432807" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    Run container in background and print container ID
  </body>
</html></richcontent>
</node>
<node TEXT="--publish, -p" ID="ID_1296420021" CREATED="1588349476452" MODIFIED="1588349514788" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    Publish a container&#8217;s port(s) to the host
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="image" FOLDED="true" ID="ID_1659008543" CREATED="1588705235229" MODIFIED="1588705328681" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="prune" FOLDED="true" ID="ID_395185191" CREATED="1588705240381" MODIFIED="1588705269986" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      remove unused images
    </p>
  </body>
</html></richcontent>
<node TEXT="--all, -a" ID="ID_205007555" CREATED="1588705269983" MODIFIED="1588705275994" COLOR="#111111"/>
</node>
</node>
<node TEXT="images" FOLDED="true" ID="ID_1116032328" CREATED="1544718622307" MODIFIED="1588173148086" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.docker.com/engine/reference/commandline/images/#options
    </p>
    <p>
      
    </p>
    <p>
      The default <code class="highlighter-rouge">docker images</code>&#160;will show all top level images, their repository and tags, and their size.
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="--all, -a" ID="ID_1529214896" CREATED="1544719245086" MODIFIED="1588705186185" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Show all images, and intermediate images
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="[REPOSITORY[:TAG]]" ID="ID_463040670" CREATED="1544719362538" MODIFIED="1588173148087" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The <code class="highlighter-rouge">docker images</code>&#xa0;command takes an optional <code class="highlighter-rouge">[REPOSITORY[:TAG]]</code>&#xa0; argument that restricts the list to images that match the argument. If you specify <code class="highlighter-rouge">REPOSITORY </code>but no <code class="highlighter-rouge">TAG</code>, the <code class="highlighter-rouge">docker images</code>&#xa0;command lists all images in the given repository.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--filter &quot;dangling=true&quot;" ID="ID_1161866150" CREATED="1588705104763" MODIFIED="1588705143694" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    Shows all unlabeled images
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="container" FOLDED="true" ID="ID_880179333" CREATED="1544718627750" MODIFIED="1588190543375" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.docker.com/engine/reference/commandline/container/
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="ls" ID="ID_537364869" CREATED="1544719430917" MODIFIED="1588173148089" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      List containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="port" ID="ID_267091808" CREATED="1544719469490" MODIFIED="1588173148089" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      List port mapping
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="rename" ID="ID_684174377" CREATED="1544719483254" MODIFIED="1588173148090" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      rename a container
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="restart" ID="ID_850758353" CREATED="1544719494259" MODIFIED="1588173148090" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      restart one or more containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="start" ID="ID_159939641" CREATED="1544719549621" MODIFIED="1588173148091" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      start one or more stopped containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="prune" ID="ID_1626919703" CREATED="1544719711036" MODIFIED="1588173148091" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      remove all stopped containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="stop" ID="ID_686139484" CREATED="1544719519436" MODIFIED="1588173148091" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Stop one or more running containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="top" ID="ID_843705869" CREATED="1544719532561" MODIFIED="1588173148092" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Display the running processes of a container
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="inspect" ID="ID_506168018" CREATED="1544719536604" MODIFIED="1588173148092" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <table>
      <tr>
        <td>
          <a href="https://docs.docker.com/engine/reference/commandline/container_inspect/">Display detailed information on one or more containers</a>
        </td>
      </tr>
      <!--
        p { margin-top: 0 }
        body { font-size: 12pt; font-family: SansSerif }
      -->
      </table>
  </body>
</html></richcontent>
</node>
<node TEXT="rm" ID="ID_145534398" CREATED="1544719538218" MODIFIED="1588173148093" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Remove one or more containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="attach" ID="ID_206804906" CREATED="1544719600260" MODIFIED="1588173148093" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      attach local standard input, output, and error streams to a running container
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="commit" ID="ID_501537982" CREATED="1544719646261" MODIFIED="1588173148094" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Create a new image from a container's changes
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="export" ID="ID_1021704235" CREATED="1544719678962" MODIFIED="1588173148094" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      export a container's filesystem as a tarball
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="run" ID="ID_1649427917" CREATED="1544719544909" MODIFIED="1588173148094" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Run a command in a new container
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="ps" FOLDED="true" ID="ID_951367319" CREATED="1544718996796" MODIFIED="1690214166640" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.docker.com/engine/reference/commandline/ps/#usage
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="--all, -a" ID="ID_516179633" CREATED="1544718999409" MODIFIED="1588173148095" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Show all containers
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--size, -s" ID="ID_1046970829" CREATED="1544719065259" MODIFIED="1690214166640" COLOR="#990000" VSHIFT_QUANTITY="-0.76596 pt">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Display total file size
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--filter, -f" FOLDED="true" ID="ID_1259363255" CREATED="1544719111977" MODIFIED="1588173148096" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<node TEXT="id" ID="ID_866706522" CREATED="1544719116451" MODIFIED="1588173148096" COLOR="#111111"/>
<node TEXT="name" ID="ID_940587918" CREATED="1544719130258" MODIFIED="1588173148097" COLOR="#111111"/>
<node TEXT="status" ID="ID_140987954" CREATED="1544719155288" MODIFIED="1588173148097" COLOR="#111111"/>
</node>
</node>
<node TEXT="build" FOLDED="true" ID="ID_1348419348" CREATED="1544719955434" MODIFIED="1588173169030" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.docker.com/engine/reference/commandline/build/
    </p>
    <p>
      
    </p>
    <p>
      <code class="language-none">docker build [OPTIONS] PATH | URL | -</code>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="--file, -f" ID="ID_939403161" CREATED="1544720332815" MODIFIED="1588173174878" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Name of the Dockerfile (default is PATH/Dockerfile)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="exec" FOLDED="true" ID="ID_594326204" CREATED="1549656203638" MODIFIED="1588173148104" LINK="https://docs.docker.com/engine/reference/commandline/exec/" COLOR="#00b439" STYLE="bubble">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <pre><code class="language-none">docker exec [OPTIONS] CONTAINER COMMAND [ARG...</code></pre>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<node TEXT="--interactive, -i" ID="ID_1206052774" CREATED="1549656274869" MODIFIED="1588173148105" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Keep STDIN open even if not attached
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--tty, -t" ID="ID_1272734713" CREATED="1549656304519" MODIFIED="1588173148105" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Allocate a pseudo-TTY
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--workdir, -w" ID="ID_1677735542" CREATED="1549656339732" MODIFIED="1588173148105" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Working directory inside the container
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="--user, -u" ID="ID_884740351" CREATED="1549656350262" MODIFIED="1588173148106" COLOR="#990000">
<font NAME="SansSerif" SIZE="14"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Username or UID (format: &lt;name|uid&gt;[:&lt;group|gid&gt;])
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="logs" ID="ID_732233274" CREATED="1588621855358" MODIFIED="1588705344068" COLOR="#00b439">
<font NAME="SansSerif" SIZE="16"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    https://docs.docker.com/config/containers/logging/ Shows information logged by a running container. Shows the command&#8217;s output just as it would appear if you ran the command interactively in a terminal.
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</map>
