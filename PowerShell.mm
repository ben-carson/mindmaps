<map version="freeplane 1.11.1">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="PowerShell" FOLDED="false" ID="ID_212219952" CREATED="1492849083851" MODIFIED="1492849094539"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Miscellaneous" POSITION="bottom_or_right" ID="ID_1786284876" CREATED="1492849101046" MODIFIED="1492849105742">
<node TEXT="refresh env" ID="ID_611610803" CREATED="1492849329046" MODIFIED="1492849352792"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      refresh environmental variable changes
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Ctrl + Break" ID="ID_1958100667" CREATED="1492849358103" MODIFIED="1492849379604"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      stop current running process
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="File System Cmdlets" POSITION="top_or_left" ID="ID_30093500" CREATED="1492849111949" MODIFIED="1492849120861">
<node TEXT="Add-Content" FOLDED="true" ID="ID_1175245789" CREATED="1492849152377" MODIFIED="1492849156886">
<node TEXT="&lt;file path&gt;" ID="ID_1674056760" CREATED="1492849317524" MODIFIED="1492849321848"/>
</node>
<node TEXT="Set-Content" FOLDED="true" ID="ID_1120742976" CREATED="1492849158234" MODIFIED="1492849163089">
<node TEXT="&lt;text string&gt;" ID="ID_1184675408" CREATED="1492849305751" MODIFIED="1492849313446"/>
</node>
<node TEXT="Remove-Item" FOLDED="true" ID="ID_901279534" CREATED="1492849163922" MODIFIED="1492849167418">
<node TEXT="-Recurse" ID="ID_1859764400" CREATED="1492849269765" MODIFIED="1492849274337"/>
<node TEXT="-Force" ID="ID_1477395100" CREATED="1492849276543" MODIFIED="1492849280725"/>
<node TEXT="&lt;folder&gt;" ID="ID_936176382" CREATED="1492849281777" MODIFIED="1492849288488"/>
</node>
<node TEXT="New-Item" FOLDED="true" ID="ID_220715873" CREATED="1492849168173" MODIFIED="1492849172043">
<node TEXT="-type" FOLDED="true" ID="ID_286409425" CREATED="1492849183383" MODIFIED="1492849189892">
<node TEXT="file" ID="ID_206062220" CREATED="1492849191209" MODIFIED="1492849194892"/>
<node TEXT="directory" ID="ID_458125240" CREATED="1492849195662" MODIFIED="1492849202029"/>
</node>
<node TEXT="&lt;name&gt;" ID="ID_1896080106" CREATED="1492849206981" MODIFIED="1492849211272"/>
<node TEXT="-force" ID="ID_371025170" CREATED="1492849213213" MODIFIED="1492849236694"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      overwrite existing
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="-value &lt;text string&gt;" ID="ID_1418271110" CREATED="1492849238465" MODIFIED="1492849261753"/>
</node>
</node>
<node TEXT="System Cmdlets" POSITION="bottom_or_right" ID="ID_635389066" CREATED="1492849123941" MODIFIED="1492849128608"/>
<node TEXT="Editing" POSITION="top_or_left" ID="ID_1960412493" CREATED="1492849131065" MODIFIED="1492849138709">
<node TEXT="# - comment" ID="ID_1707269501" CREATED="1492849140323" MODIFIED="1492849147219"/>
</node>
</node>
</map>
