<map version="freeplane 1.11.1">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="PostgreSQL" FOLDED="false" ID="ID_709380100" CREATED="1498527132521" MODIFIED="1498527187829"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="information" POSITION="top_or_left" ID="ID_69332993" CREATED="1498527190108" MODIFIED="1498542231867"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      tools and commands for finding information about a postgres instance
    </p>
  </body>
</html></richcontent>
<node TEXT="\l - Lists all the databases" ID="ID_798374348" CREATED="1498540706634" MODIFIED="1498541846740"/>
<node TEXT="\dn - list schemas" ID="ID_860203827" CREATED="1498540799969" MODIFIED="1498541873691"/>
<node TEXT="\dt - List tables in connected database" ID="ID_634281583" CREATED="1498540763449" MODIFIED="1498541855243"/>
<node TEXT="\d {table} - list columns on table" ID="ID_1794343446" CREATED="1498540818109" MODIFIED="1498541886456"/>
<node TEXT="\du - lists roles" ID="ID_181313373" CREATED="1498527155475" MODIFIED="1498541831344"/>
<node TEXT="\dv - List views" ID="ID_562083538" CREATED="1498540861230" MODIFIED="1498541907852"/>
<node TEXT="\x - pretty-format query results" ID="ID_271462220" CREATED="1498540877629" MODIFIED="1498541933948"/>
</node>
<node TEXT="create" POSITION="bottom_or_right" ID="ID_1398759736" CREATED="1498540914256" MODIFIED="1498542160459">
<node TEXT="CREATE USER {username} WITH PASSWORD &apos;{password}&apos;;" ID="ID_1685750348" CREATED="1498540935990" MODIFIED="1498540969043"/>
<node TEXT="CREATE DATABASE {dbname} OWNER {username};" ID="ID_1339849147" CREATED="1498540970487" MODIFIED="1498540998194"/>
<node TEXT="CREATE SCHEMA {schemaname};" ID="ID_806065269" CREATED="1498540999981" MODIFIED="1498541052770"/>
<node TEXT="CREATE OR REPLACE VIEW {viewname} AS {select_statement}" ID="ID_440670913" CREATED="1498542160460" MODIFIED="1498542189868"/>
</node>
<node TEXT="permissions" POSITION="bottom_or_right" ID="ID_1956132326" CREATED="1498541060222" MODIFIED="1498541128475">
<node TEXT="GRANT {privilege} ON DATABASE {dbname} TO {username};" ID="ID_1834770405" CREATED="1498541221519" MODIFIED="1498541540603"/>
<node TEXT="GRANT {privilege} ON SCHEMA {schemaname} to {username};" ID="ID_377917449" CREATED="1498541065429" MODIFIED="1498541551029"/>
<node TEXT="PRIVILEGES" ID="ID_672124675" CREATED="1498541446670" MODIFIED="1498541517126">
<node TEXT="SELECT" ID="ID_1162703694" CREATED="1498541463142" MODIFIED="1498541468971"/>
<node TEXT="INSERT" ID="ID_1075169233" CREATED="1498541469661" MODIFIED="1498541472027"/>
<node TEXT="UPDATE" ID="ID_62101749" CREATED="1498541472488" MODIFIED="1498541474509"/>
<node TEXT="DELETE" ID="ID_598461377" CREATED="1498541474886" MODIFIED="1498541477443"/>
<node TEXT="RULE" ID="ID_1151119125" CREATED="1498541477878" MODIFIED="1498541479947"/>
<node TEXT="REFERENCES" ID="ID_1852313837" CREATED="1498541480430" MODIFIED="1498541484122"/>
<node TEXT="TRIGGER" ID="ID_979788452" CREATED="1498541484558" MODIFIED="1498541486138"/>
<node TEXT="CREATE" ID="ID_1501362771" CREATED="1498541486838" MODIFIED="1498541488050"/>
<node TEXT="TEMPORARY" ID="ID_1287137354" CREATED="1498541488638" MODIFIED="1498541491267"/>
<node TEXT="EXECUTE" ID="ID_426735860" CREATED="1498541491782" MODIFIED="1498541494987"/>
<node TEXT="USAGE" ID="ID_1441104239" CREATED="1498541495382" MODIFIED="1498541496859"/>
<node TEXT="ALL PRIVILEGES" ID="ID_602863848" CREATED="1498541518366" MODIFIED="1498541525700"/>
</node>
</node>
<node TEXT="administration" POSITION="top_or_left" ID="ID_1651900126" CREATED="1498542248015" MODIFIED="1498542254131">
<node TEXT="pg_dump - utility for backup up a database" FOLDED="true" ID="ID_1274896298" CREATED="1498542255598" MODIFIED="1690214205668">
<node TEXT="pg_dump {dbname} &gt; {filename}.sql - plaintext" ID="ID_799915685" CREATED="1498542275598" MODIFIED="1498542319452"/>
<node TEXT="pg_dump -Fc {dbname} &gt; {filename}.bak - compressed binary format" ID="ID_532623269" CREATED="1498542320278" MODIFIED="1690214205667"/>
<node TEXT="pg_dump -Ft {dbname} &gt; {filename}.tar - tarball" ID="ID_813255005" CREATED="1498542363086" MODIFIED="1498542385692"/>
</node>
<node TEXT="pg_restore - utility for restoring a database" FOLDED="true" ID="ID_1550925168" CREATED="1498542395839" MODIFIED="1498546302543">
<node TEXT="pg_restore -Fc {filename}.bak - restore existing db from compressed binary" ID="ID_965172530" CREATED="1498546305226" MODIFIED="1498546393334"/>
<node TEXT="pg_restore -Ft {filename}.tar - restore existing db from tarball" ID="ID_946936457" CREATED="1498546345017" MODIFIED="1498546384182"/>
<node TEXT="pg_restore -Fc -C {filename}.bak - create new db from compressed binary backup" ID="ID_491680530" CREATED="1498546395506" MODIFIED="1498546503590"/>
<node TEXT="pg_restore -Ft -C {filename}.tar - create new db from tarball backup" ID="ID_162373549" CREATED="1498546454889" MODIFIED="1498546496208"/>
</node>
</node>
<node TEXT="connections" POSITION="bottom_or_right" ID="ID_1631829211" CREATED="1498546545763" MODIFIED="1498546764774">
<node TEXT="psql" FOLDED="true" ID="ID_1041632895" CREATED="1498546555354" MODIFIED="1498547264879">
<node TEXT="-h {hostname} -p {port} -U {username} {dbname}" ID="ID_1385182743" CREATED="1498547267858" MODIFIED="1498547272311"/>
<node TEXT="-h - host" ID="ID_611228718" CREATED="1498546585667" MODIFIED="1498546595078"/>
<node TEXT="-p - port to connect on" ID="ID_1645341335" CREATED="1498546605298" MODIFIED="1498546612822"/>
<node TEXT="-U - user to connect with" ID="ID_1781166960" CREATED="1498546595593" MODIFIED="1498546604684"/>
<node TEXT="&quot;dbname={dbname} host={host} user={username} password={password} port=5432 sslmode=require&quot;" ID="ID_1948955099" CREATED="1498547283746" MODIFIED="1498547348583"/>
</node>
<node TEXT="\c {dbname} - connect to another database" ID="ID_161071136" CREATED="1498546732850" MODIFIED="1498546749918"/>
<node TEXT="\q - quit" ID="ID_1806509827" CREATED="1498546768922" MODIFIED="1498546772703"/>
</node>
</node>
</map>
