If you wish to contribute to this project, just make sure it's formatted for use in [FreeMind](http://freemind.sourceforge.net/wiki/index.php/Download).

I have found that [freemup](https://www.mindmup.com/) is a great online tool for mindmaps. It is free, saves to a variety of locations (including dropbox and Github), and works with Freemind.