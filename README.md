Mindmap collections
---

Collection of personal mindmaps. Some are for coding and technical subjects, others are from when I was brainstorming business ideas.

The are in .mm format, readable with [FreePlane](https://docs.freeplane.org/).

