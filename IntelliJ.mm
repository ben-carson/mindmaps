<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="IntelliJ" FOLDED="false" ID="ID_735631788" CREATED="1454103961626" MODIFIED="1547747844956"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      
    </p>
    <table border="0" style="border-left-style: solid; border-top-style: solid; border-top-width: 0; width: 80%; border-left-width: 0; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 0; border-right-width: 0">
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#8984;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Command
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#8997;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Option
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            ^
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Control
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#8679;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Shift
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#8677;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Tab
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#9166;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Return
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">&#9003;</font>
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Delete
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            &#8593;
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">Up</font>
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            &#8595;
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            <font face="Helvetica Neue, Helvetica, Arial, sans-serif" color="rgb(55, 55, 55)">Down</font>
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            &#8592;
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Left
          </p>
        </td>
      </tr>
      <tr>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            &#8594;
          </p>
        </td>
        <td style="border-left-style: solid; border-top-style: solid; border-top-width: 1; width: 50%; border-left-width: 1; border-bottom-style: solid; border-right-style: solid; border-bottom-width: 1; border-right-width: 1" valign="top">
          <p style="margin-right: 1; margin-bottom: 1; margin-left: 1; margin-top: 1">
            Right
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Cursor Movement" POSITION="right" ID="ID_535886595" CREATED="1454103978951" MODIFIED="1454103985275">
<node TEXT="⇧⌘↑ = Move current line up" ID="ID_1502845662" CREATED="1454104093963" MODIFIED="1454343313011"/>
<node TEXT="⇧⌘↓ = Move current line down" ID="ID_331627559" CREATED="1454104958396" MODIFIED="1454343308588"/>
</node>
<node TEXT="Cut &amp; Paste" POSITION="left" ID="ID_1405078312" CREATED="1454104002969" MODIFIED="1454104007484">
<node TEXT="⌘d = Duplicate a line" ID="ID_186694973" CREATED="1454354887376" MODIFIED="1454355931418"/>
<node TEXT="⌘r = Find &amp; replace in the currently open file" ID="ID_768547181" CREATED="1454340822283" MODIFIED="1454343581735"/>
</node>
<node TEXT="Editing" POSITION="right" ID="ID_1980484031" CREATED="1454104010524" MODIFIED="1454104032011">
<node TEXT="" ID="ID_1265318585" CREATED="1454104989962" MODIFIED="1454354884744"/>
<node TEXT="⌘⌫ = Remove a line" ID="ID_628509047" CREATED="1454104999227" MODIFIED="1454343301036"/>
<node TEXT="⌘/ = Toggle comment on a line" ID="ID_569031057" CREATED="1454105013658" MODIFIED="1454343295940"/>
<node TEXT="⇧⌘/ = Toggle comment on a block of text" ID="ID_1917541705" CREATED="1454105027024" MODIFIED="1454343291916"/>
<node TEXT="⌘+ = Expand code block" ID="ID_1969441317" CREATED="1454340927787" MODIFIED="1454343287604"/>
<node TEXT="⌘- = Collapse code block" ID="ID_472013047" CREATED="1454340938544" MODIFIED="1454343327651"/>
<node TEXT="⌘n = Create new..." ID="ID_829473097" CREATED="1454340948288" MODIFIED="1454343346091"/>
<node TEXT="⌥⌘t = Surround with..." ID="ID_162368632" CREATED="1454340954665" MODIFIED="1454343367287"/>
<node TEXT="fn⇧⌘F7 = Highlight usages of a symbol" ID="ID_106778694" CREATED="1454340964815" MODIFIED="1454358642924"/>
<node TEXT="⌥⇧+↑ or ↓ = Move line up or down" ID="ID_1884181916" CREATED="1547578801517" MODIFIED="1547579033988"/>
</node>
<node TEXT="Navigation" POSITION="left" ID="ID_457328910" CREATED="1454340694862" MODIFIED="1454340699790">
<node TEXT="⌘⇧[/⌘⇧] = Navigate between open tabs" ID="ID_370767154" CREATED="1454340702591" MODIFIED="1454354622062"/>
<node TEXT="⌘[ = Navigate back in history" ID="ID_1482440805" CREATED="1454340762130" MODIFIED="1454354635105"/>
<node TEXT="⌘] = Navigate forward in history" ID="ID_1190002642" CREATED="1454340881305" MODIFIED="1454354639421"/>
<node TEXT="⌘e = Recent files" ID="ID_208147855" CREATED="1454356030674" MODIFIED="1454356050956"/>
<node TEXT="⌘o = Navigate to Class" ID="ID_165118338" CREATED="1454356068780" MODIFIED="1454356079915"/>
<node TEXT="⌘⇧o = Navigate to File/Folder" ID="ID_698181153" CREATED="1454356090060" MODIFIED="1454356166295"/>
<node TEXT="⌥⌘o = Navigate to Symbol" ID="ID_1343217563" CREATED="1454356276102" MODIFIED="1454356315206"/>
<node TEXT="fn⌘F12 = Navigate file structure (for current file)" ID="ID_36301104" CREATED="1454356382269" MODIFIED="1459542725526"/>
<node TEXT="⌘b = Navigate to declaration" ID="ID_1652637640" CREATED="1454358298466" MODIFIED="1454358333621"/>
<node TEXT="^h = Navigate type heirarchy" ID="ID_1676513381" CREATED="1454358339083" MODIFIED="1454358360772"/>
<node TEXT="fn⌥⌘F7 = Show usages" ID="ID_799889604" CREATED="1454358515089" MODIFIED="1454358563003"/>
<node TEXT="⌥⌘b = Show implementation" ID="ID_1542035653" CREATED="1454358534423" MODIFIED="1454358568306">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node TEXT="⌘+ = Expand code block" ID="ID_1802260817" CREATED="1459543088801" MODIFIED="1459543108632"/>
<node TEXT="⌘- = Collapse code block" ID="ID_391805963" CREATED="1459543109384" MODIFIED="1459543121517"/>
</node>
<node TEXT="Search" POSITION="right" ID="ID_1218623089" CREATED="1454340789020" MODIFIED="1454340792717">
<node TEXT="⌘f = Find in currently open file" ID="ID_1214939227" CREATED="1454340792718" MODIFIED="1454343573447"/>
<node TEXT="⇧⌘g = Previous Occurrence" ID="ID_1231284114" CREATED="1454340826189" MODIFIED="1454343640486"/>
<node TEXT="⌘g = Next Occurrence" ID="ID_821362879" CREATED="1454340842689" MODIFIED="1454343596199"/>
<node TEXT="⇧⌘a = Search for a shortcut" ID="ID_1250722077" CREATED="1454343243257" MODIFIED="1454343635629"/>
<node TEXT="⇧(x2) = Search everywhere" ID="ID_565701738" CREATED="1459543132804" MODIFIED="1459543162738"/>
<node TEXT="fn⌥F7 = Find usages" ID="ID_1824409699" CREATED="1459543177063" MODIFIED="1459543203117"/>
</node>
<node TEXT="Selecting Text" POSITION="left" ID="ID_1210180126" CREATED="1454343706156" MODIFIED="1454354896810">
<node TEXT="^g = Multi-select next occurrence of current work" ID="ID_45344311" CREATED="1454343712114" MODIFIED="1454355876904"/>
<node TEXT="Code Completion" FOLDED="true" ID="ID_1834665891" CREATED="1459542740810" MODIFIED="1459542747705">
<node TEXT="^Space = Basic Completion" ID="ID_895798122" CREATED="1459542747707" MODIFIED="1459542769709"/>
<node TEXT="Tab = Overwrite identifier (instead of insert)" ID="ID_557433619" CREATED="1459542770531" MODIFIED="1459542792610"/>
<node TEXT="⇧⌘⏎ = Auto-complete statement" ID="ID_920908323" CREATED="1459542794945" MODIFIED="1459542861351"/>
</node>
</node>
<node TEXT="Automation" POSITION="left" ID="ID_685774349" CREATED="1459542870440" MODIFIED="1459543004888">
<node TEXT="⌥⌘L = Reformat Code" ID="ID_1548744457" CREATED="1459542875573" MODIFIED="1459542922166"/>
<node TEXT="^⌥i = Auto-indent lines" ID="ID_424824643" CREATED="1459542892598" MODIFIED="1459542965964"/>
<node TEXT="^⌥o = Optimize imports" ID="ID_338121960" CREATED="1459542894350" MODIFIED="1459542993037"/>
</node>
<node TEXT="Miscellaneous" POSITION="right" ID="ID_1799691298" CREATED="1454357167835" MODIFIED="1454357172522">
<node TEXT="⌥⌘u = Show UML popup " ID="ID_1012954662" CREATED="1454357174098" MODIFIED="1459543242380"/>
<node TEXT="fn⌥F12 = Terminal" ID="ID_1490862690" CREATED="1459543211133" MODIFIED="1459543231964"/>
</node>
<node TEXT="Refactoring" POSITION="right" ID="ID_428206384" CREATED="1459543009609" MODIFIED="1459543029225">
<node TEXT="fn⇧F6 = Rename" ID="ID_1310775813" CREATED="1459543029226" MODIFIED="1459543054154"/>
</node>
</node>
</map>
