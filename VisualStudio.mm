<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Visual Studio" FOLDED="false" ID="1" CREATED="1492848827218" MODIFIED="1612904282147"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Code" POSITION="right" ID="5" CREATED="1492848827218" MODIFIED="1492848827218">
<node TEXT="Comment line - Ctrl+K,Ctrl+C" ID="ID_1918050223" CREATED="1495787508588" MODIFIED="1612564825648"/>
<node TEXT="Uncomment line - Ctrl+K,Ctrl+U" ID="ID_1507316717" CREATED="1495787508588" MODIFIED="1612564816742"/>
</node>
<node TEXT="Editing" POSITION="right" ID="2" CREATED="1492848827218" MODIFIED="1495787353932">
<node TEXT="Rename - Ctrl + [R (x2)]" ID="ID_949201164" CREATED="1492848989677" MODIFIED="1612564658547"/>
<node TEXT="Duplicate Line Below - Ctrl+D" ID="ID_546429402" CREATED="1495787356825" MODIFIED="1612564835897"/>
<node TEXT="Move line up - Alt + Up" ID="ID_21221082" CREATED="1495787542201" MODIFIED="1612565159469"/>
<node TEXT="Move line down - Alt + Down" ID="ID_1331192368" CREATED="1495787590874" MODIFIED="1612565165963"/>
<node TEXT="Dupe cursor on mult rows - Ctrl+Shift+Click" ID="ID_1803877771" CREATED="1612966142722" MODIFIED="1612966180641"/>
<node TEXT="Select multi-row - Alt+click-&gt;drag" ID="ID_1554424567" CREATED="1612966184268" MODIFIED="1612966212969"/>
</node>
<node TEXT="Navigation " POSITION="left" ID="4" CREATED="1492848827218" MODIFIED="1492848909267">
<node TEXT="Go to Code from Designer - F7" ID="ID_1388597816" CREATED="1492848884291" MODIFIED="1612565010076"/>
<node TEXT="Go to Designer from Code - Shift+F7" ID="ID_276805303" CREATED="1492848928887" MODIFIED="1612565020360"/>
<node TEXT="Jump to matching brace - Ctrl + { ( or } )" ID="ID_1856493547" CREATED="1495787427161" MODIFIED="1612564983482"/>
<node TEXT="Go to Definition - F12" ID="ID_1350548949" CREATED="1612965968391" MODIFIED="1612965983111"/>
</node>
</node>
</map>
