<map version="freeplane 1.11.5">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="vi" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_294281872" CREATED="1465241509393" MODIFIED="1663943972547"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent TYPE="DETAILS" CONTENT-TYPE="plain/auto"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Editing" LOCALIZED_STYLE_REF="default" POSITION="bottom_or_right" ID="ID_1317403400" CREATED="1501639557888" MODIFIED="1663944824687">
<cloud COLOR="#fda829" SHAPE="ROUND_RECT"/>
<node TEXT="Selection" ID="ID_1259623083" CREATED="1501640024472" MODIFIED="1663944797757"><richcontent TYPE="NOTE">
<html>
  <head>
    
  </head>
  <body>
    <p>
      On Windows, one can use the mouse for selection of text
    </p>
  </body>
</html></richcontent>
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
<node TEXT="visual mode - v (then move cursor)" ID="ID_670547452" CREATED="1723209405866" MODIFIED="1723209429739"/>
</node>
<node TEXT="deletion" LOCALIZED_STYLE_REF="default" ID="ID_1343449559" CREATED="1566923923155" MODIFIED="1663945349486">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="Delete char - x" LOCALIZED_STYLE_REF="default" ID="ID_1824329456" CREATED="1566923978286" MODIFIED="1663944824686"/>
<node TEXT="delete word - dw" LOCALIZED_STYLE_REF="default" ID="ID_83550198" CREATED="1566923995182" MODIFIED="1663944824686"/>
<node TEXT="delete line - dd" LOCALIZED_STYLE_REF="default" ID="ID_1821333178" CREATED="1566924362061" MODIFIED="1663944824685"/>
<node TEXT="Join lines - J" LOCALIZED_STYLE_REF="default" ID="ID_655586009" CREATED="1566924771677" MODIFIED="1663944824685"/>
<node TEXT="Delete current line to EOF - dG" LOCALIZED_STYLE_REF="default" ID="ID_1405926784" CREATED="1588017018265" MODIFIED="1663944824684"/>
<node TEXT="Delete cursor to EOF - dCtrl+End" LOCALIZED_STYLE_REF="default" ID="ID_594154170" CREATED="1588017070873" MODIFIED="1663944824684"/>
</node>
<node TEXT="Insertion" LOCALIZED_STYLE_REF="default" ID="ID_750688382" CREATED="1566924030271" MODIFIED="1663945381790">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="paste after cursor - :p" ID="ID_1393237602" CREATED="1566924035047" MODIFIED="1566924103676"/>
<node TEXT="paste before cursor - :P" ID="ID_135409343" CREATED="1566924072776" MODIFIED="1566924124268"/>
</node>
<node TEXT="Copy" ID="ID_1701686792" CREATED="1501639750519" MODIFIED="1663945395834">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="y: yank" ID="ID_1421940250" CREATED="1603909419816" MODIFIED="1603909447527"/>
<node TEXT="y{motion}: yank can be combined with any valid Vim motion" ID="ID_1647612004" CREATED="1603909459589" MODIFIED="1663944641282">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</node>
<node TEXT="yy or Y: yank the currentline, new line character included" ID="ID_1796299781" CREATED="1603909474981" MODIFIED="1603909477455"/>
<node TEXT="y$: yank the current line, new line character excluded" ID="ID_1895555089" CREATED="1603909491880" MODIFIED="1603909494704"/>
<node TEXT="yiw: yank current word, excluding surrounding whitespace" ID="ID_994451785" CREATED="1603909509653" MODIFIED="1603909512653"/>
<node TEXT="yaw: yank current word, including surrounding whitespace" ID="ID_892418065" CREATED="1603909521127" MODIFIED="1603909523213"/>
<node TEXT="ytx: (yank til x) yank from the current position up to and before the character" ID="ID_988224657" CREATED="1603909534053" MODIFIED="1603909536784"/>
<node TEXT="yfx: (yank find x) yank from current position up to and including the character" ID="ID_54933031" CREATED="1603909548812" MODIFIED="1603909551507"/>
</node>
<node TEXT="Basic" ID="ID_20614972" CREATED="1566924273264" MODIFIED="1663945406233">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="Insert mode - i" ID="ID_1298416000" CREATED="1566924292948" MODIFIED="1566924311288"/>
<node TEXT="Normal mode - Esc" ID="ID_253812040" CREATED="1566924313110" MODIFIED="1566924321320"/>
<node TEXT="Insert at EOF - Go" ID="ID_147802703" CREATED="1588018571971" MODIFIED="1588018583551"/>
</node>
</node>
<node TEXT="File I/O" POSITION="bottom_or_right" ID="ID_842440957" CREATED="1501639919128" MODIFIED="1663944733843">
<cloud COLOR="#02cc15" SHAPE="ROUND_RECT"/>
<node TEXT="Save - :q" ID="ID_1553783230" CREATED="1501639927335" MODIFIED="1566924016026"/>
<node TEXT="Open" ID="ID_1641003959" CREATED="1501639931823" MODIFIED="1501639934164"/>
</node>
<node TEXT="Motion" POSITION="top_or_left" ID="ID_1543802597" CREATED="1501639907719" MODIFIED="1663945488295" COLOR="#fdfdfd">
<cloud COLOR="#fb61b1" SHAPE="ROUND_RECT"/>
<node TEXT="Move Up - k" ID="ID_1429346848" CREATED="1663945414164" MODIFIED="1663945618789" COLOR="#ffffff"/>
<node TEXT="Move Down - j" ID="ID_600445860" CREATED="1663945498459" MODIFIED="1663945615261"/>
<node TEXT="Move Left - h" ID="ID_1591740192" CREATED="1663945510457" MODIFIED="1663945571565"/>
<node TEXT="Move Right - l" ID="ID_1142331786" CREATED="1663945519108" MODIFIED="1663945580172"/>
<node TEXT="Move BOL - 0" ID="ID_758616148" CREATED="1723144140908" MODIFIED="1723144152747"/>
<node TEXT="Move 1st non-blank char of line - ^" ID="ID_1105802215" CREATED="1723144154533" MODIFIED="1723144169334"/>
<node TEXT="Move EOL - $" ID="ID_658837321" CREATED="1723144076597" MODIFIED="1723144085665"/>
</node>
<node TEXT="Interface" POSITION="top_or_left" ID="ID_1497148481" CREATED="1501655339527" MODIFIED="1663944696279">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="coloring" ID="ID_411089560" CREATED="1501655344948" MODIFIED="1501655532497"><richcontent TYPE="NOTE">
<html>
  <head>
    
  </head>
  <body>
    <p>
      :colorscheme &lt;schemename&gt;
    </p>
    <p>
      
    </p>
    <p>
      currently known names
    </p>
    <ul>
      <li>
        morning
      </li>
      <li>
        elflord
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Combos" POSITION="bottom_or_right" ID="ID_744023524" CREATED="1588192168478" MODIFIED="1663944778355">
<cloud COLOR="#fb88ff" SHAPE="ROUND_RECT"/>
<node TEXT="Move current line up one line - ddkP" ID="ID_1085964288" CREATED="1588192177977" MODIFIED="1588252183645"/>
<node TEXT="Move current line down one line - ddp" ID="ID_1323908915" CREATED="1588192198630" MODIFIED="1588192211508"/>
</node>
</node>
</map>
