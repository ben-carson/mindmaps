<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="tmux" FOLDED="false" ID="ID_411212733" CREATED="1584722945379" MODIFIED="1584728935161" COLOR="#00ff00" BACKGROUND_COLOR="#000000" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false" show_icon_for_attributes="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="sessions" POSITION="right" ID="ID_1703883396" CREATED="1584722965022" MODIFIED="1584728901927" COLOR="#ffffff" BACKGROUND_COLOR="#21de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="new" ID="ID_1352033206" CREATED="1584723042406" MODIFIED="1584728866003" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="tmux" ID="ID_866871474" CREATED="1584722992301" MODIFIED="1584728866000" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux new" ID="ID_1858018010" CREATED="1584723006645" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux new-session" ID="ID_884763606" CREATED="1584722997005" MODIFIED="1584728866001" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux new -s sessionname" ID="ID_403972428" CREATED="1584723023869" MODIFIED="1584728866001" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="attach" ID="ID_15929818" CREATED="1584723047325" MODIFIED="1584728866003" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="tmux a" ID="ID_453774886" CREATED="1584723082246" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux att" ID="ID_1080822677" CREATED="1584723094454" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux attach" ID="ID_1238603980" CREATED="1584723086838" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux attach-session" ID="ID_1953285232" CREATED="1584723116999" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux a -t sessionname" ID="ID_1150514751" CREATED="1584723132687" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="remove" ID="ID_157954078" CREATED="1584723050309" MODIFIED="1584728866003" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="tmux kill-ses" ID="ID_1946501212" CREATED="1584723142631" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="tmux kill-session -t sessionname" ID="ID_534186078" CREATED="1584723149527" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="key bindings" ID="ID_203087717" CREATED="1584723054366" MODIFIED="1584728866003" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="CTRL+B,$ - rename session" ID="ID_138956579" CREATED="1584723165680" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B,D - detach session" ID="ID_1645007413" CREATED="1584723183144" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B,) - next session" ID="ID_1097228112" CREATED="1584723205128" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B,( - previous session" ID="ID_1109341007" CREATED="1584723219304" MODIFIED="1584728866002" COLOR="#ffffff" BACKGROUND_COLOR="#13de00" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
</node>
<node TEXT="windows" POSITION="left" ID="ID_355514078" CREATED="1584722971236" MODIFIED="1584728757737" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Windows are like tabs in a browser. Windows exist in sessions and occupy the space of a session screen.
    </p>
  </body>
</html></richcontent>
<edge WIDTH="4"/>
<node TEXT="CTRL+B, C - create window" ID="ID_1113036388" CREATED="1584723502680" MODIFIED="1584728777823" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, N - move to next window" ID="ID_1384610321" CREATED="1584723257913" MODIFIED="1584728777825" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, P - move to prev window" ID="ID_601565634" CREATED="1584723728449" MODIFIED="1584728777825" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, L - move to last window" ID="ID_303203464" CREATED="1584723752434" MODIFIED="1584728777825" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, 0...9,  select window by number" ID="ID_1117616688" CREATED="1584723836268" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, &apos; - select window by name" ID="ID_1526820594" CREATED="1584723881780" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, . - change window number" ID="ID_1289009801" CREATED="1584723898796" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, , - rename window" ID="ID_1545464357" CREATED="1584723914909" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, F - search windows" ID="ID_1041722382" CREATED="1584723940517" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, &amp; - kill window" ID="ID_1573055008" CREATED="1584723958349" MODIFIED="1584728777824" COLOR="#ffffff" BACKGROUND_COLOR="#de0022" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="panes" POSITION="left" ID="ID_244051994" CREATED="1584722975860" MODIFIED="1584727767993" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Panes are sections of windows that have been split into different screens - just like the panes of a real window!
    </p>
  </body>
</html></richcontent>
<edge WIDTH="4"/>
<node ID="ID_1624638621" CREATED="1584725316623" MODIFIED="1587999695599" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      CTRL+b, % - split pane vertically
    </p>
  </body>
</html>
</richcontent>
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node ID="ID_218456609" CREATED="1584725330398" MODIFIED="1587999664622" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      CTRL+B, &quot; - horizontal split
    </p>
  </body>
</html>
</richcontent>
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, -&gt; - move to the pane to the right" ID="ID_330317106" CREATED="1584725349582" MODIFIED="1584728834178" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, &lt;- - move ot the pane to the left" ID="ID_448275258" CREATED="1584725367639" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, up arrow - Move up to pane" ID="ID_57216698" CREATED="1584725392279" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, down arrow - move down to pane" ID="ID_492619323" CREATED="1584725418687" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, O - go to next pane" ID="ID_583085561" CREATED="1584725448472" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, ; - go to last active pane" ID="ID_1674988709" CREATED="1584725472648" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, } - move pane right" ID="ID_960301877" CREATED="1584725499089" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, { - move pane left" ID="ID_1010966556" CREATED="1584725511049" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, ! - convert pane to window" ID="ID_1698286330" CREATED="1584725532506" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="CTRL+B, X - kill pane" ID="ID_1770942729" CREATED="1584725552563" MODIFIED="1584728834177" COLOR="#ffffff" BACKGROUND_COLOR="#006fde" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="copy mode" POSITION="right" ID="ID_442425875" CREATED="1584722979836" MODIFIED="1584727784097" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="CTRL+B, [ - enter copy mode" ID="ID_315945453" CREATED="1584725581250" MODIFIED="1584728683757" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
<node TEXT="space - start selection" ID="ID_1093549117" CREATED="1584725636828" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="enter - copy selection" ID="ID_1766875133" CREATED="1584725648787" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="esc - clear selection" ID="ID_449394372" CREATED="1584725657748" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="g - go to top" ID="ID_814581019" CREATED="1584725667564" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="G - go to bottom" ID="ID_729192115" CREATED="1584725675940" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="h - move cursor left" ID="ID_1876825277" CREATED="1584725682084" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="j - move cursor down" ID="ID_1640735767" CREATED="1584725690309" MODIFIED="1584728683759" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="k - move cursor up" ID="ID_1791590459" CREATED="1584725723173" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="l - move cursor right" ID="ID_1905015876" CREATED="1584725737373" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="/ - search" ID="ID_1712232047" CREATED="1584725747397" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="# - list paste buffers" ID="ID_521503273" CREATED="1584725751853" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
<node TEXT="q - quit" ID="ID_1872294517" CREATED="1584725762501" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
<node TEXT="CTRL+B, ] - paste from buffer" ID="ID_1136096033" CREATED="1584725610563" MODIFIED="1584728683758" COLOR="#ffffff" BACKGROUND_COLOR="#de00dc" STYLE="bubble">
<font NAME="Open Sans Semibold" SIZE="18"/>
<edge WIDTH="4"/>
</node>
</node>
</node>
</map>
