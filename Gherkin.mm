<map version="freeplane 1.11.5">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Gherkin" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1707761357251" LINK="https://cucumber.io/docs/gherkin/reference/#keywords" VGAP_QUANTITY="3 pt"><hook NAME="MapStyle" background="#d6e8e8" zoom="0.826">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" associatedTemplateLocation="template:/light_sky_element_template.mm" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_4172522" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#051552" BACKGROUND_COLOR="#5cd5e8" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.7 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#1164b0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_4172522" STARTINCLINATION="116.22048 pt;0 pt;" ENDINCLINATION="116.22048 pt;28.34646 pt;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#051552" WIDTH="2" DASH="SOLID"/>
<richcontent TYPE="DETAILS" CONTENT-TYPE="plain/auto"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#fff024" BACKGROUND_COLOR="#000000"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#000000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#ffffff" BACKGROUND_COLOR="#cc7212" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#1164b0"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_1823054225" COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#9d5e19">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#9d5e19" TRANSPARENCY="255" DESTINATION="ID_1823054225"/>
<font SIZE="11" BOLD="true"/>
<edge COLOR="#9d5e19"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#053d8b" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#1164b0" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#298bc8" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#ffffff" BACKGROUND_COLOR="#3fb7db" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#051552" BACKGROUND_COLOR="#5cd5e8" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font BOLD="true"/>
<node TEXT="Basic" POSITION="bottom_or_right" ID="ID_1857669736" CREATED="1707259056597" MODIFIED="1707761080752">
<node TEXT="Feature:" ID="ID_1867514159" CREATED="1707259066779" MODIFIED="1707761422358"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      After the colon an optional description of the feature can be provided
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Scenario:&#xa;or&#xa;Example:" ID="ID_375691310" CREATED="1707259072336" MODIFIED="1707761438846">
<node TEXT="GIVEN" ID="ID_27591595" CREATED="1707259085211" MODIFIED="1707347254419"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The state of the system before the test begins
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="WHEN" ID="ID_304063378" CREATED="1707259090303" MODIFIED="1707346836780"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The action to be performed/exercised
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="THEN" ID="ID_252438709" CREATED="1707259092898" MODIFIED="1707346860870"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The expected output/assertion of the test
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="AND" ID="ID_1044583606" CREATED="1707259096153" MODIFIED="1707259099041"/>
<node TEXT="BUT" ID="ID_1536390610" CREATED="1707259099776" MODIFIED="1707346747938"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Functionally the same as AND,
    </p>
    <p>
      however semantically it makes more
    </p>
    <p>
      sense sometimes to use this negation
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="*" ID="ID_1300435796" CREATED="1707259102126" MODIFIED="1707346812398"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Functionally the same as AND,
    </p>
    <p>
      but useful when having lists
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="Repeatable" POSITION="top_or_left" ID="ID_984174297" CREATED="1707259114337" MODIFIED="1707259127411">
<node TEXT="Background:" ID="ID_594497412" CREATED="1707259128583" MODIFIED="1707761413992"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      if all scenarios of a feature have
    </p>
    <p>
      the same Given, they can be put into
    </p>
    <p>
      a Background block to reduce typing
    </p>
    <p>
      and repeating Given blocks
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Scenario Outline:&#xa;or&#xa;Scenario Template:" ID="ID_1493977846" CREATED="1707259476076" MODIFIED="1707761447388">
<node TEXT="variables in angle brackets, &lt;...&gt;" ID="ID_706719211" CREATED="1707259491303" MODIFIED="1707346564287"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Given ... &lt;input&gt;
    </p>
    <p>
      When ... &lt;newVal&gt;
    </p>
    <p>
      Then ... &lt;expectedVal&gt;
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="create data table for outline" ID="ID_255271066" CREATED="1707260424711" MODIFIED="1707346601354"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      EXAMPLES:
    </p>
    <p>
      |input|newVal|expectedVal|
    </p>
    <p>
      | 1&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;|&#xa0;&#xa0;&#xa0;a&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;| &quot;happy&quot;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;|
    </p>
    <p>
      | 14&#xa0;&#xa0;&#xa0;|&#xa0;&#xa0;&#xa0;b&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;| &quot;sad&quot;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;|
    </p>
    <p>
      | ...&#xa0;&#xa0;&#xa0;&#xa0;|&#xa0;&#xa0;...&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;| ...&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;|
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="Secondary&#xa;keywords" POSITION="bottom_or_right" ID="ID_167193905" CREATED="1707760983465" MODIFIED="1707760994541">
<node TEXT="&quot;&quot;&quot;" ID="ID_1548930491" CREATED="1707760995588" MODIFIED="1707761003301"/>
<node TEXT="|" ID="ID_370516474" CREATED="1707761004150" MODIFIED="1707761007001"/>
<node TEXT="@" ID="ID_983662240" CREATED="1707761008730" MODIFIED="1707761011644"/>
<node TEXT="#" ID="ID_448660227" CREATED="1707761012278" MODIFIED="1707761015025"/>
<node TEXT="Rule:" ID="ID_985470382" CREATED="1707761223468" MODIFIED="1707761252523"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      represent one business rule that should be implemented
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
